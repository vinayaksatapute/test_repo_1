terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "Vinayak_CloudRoom"
  region  = "ap-southeast-1"
}

resource "aws_instance" "test-ec2-server" {
  ami           = "ami-0f9d733050c9f5365"
  instance_type = var.ec2_instance_type
  vpc_security_group_ids = ["sg-020a46ce2aa5095e9", "sg-04155309d4b2dd5a1"]
  subnet_id              = "subnet-06a536e084f76275c"

  tags = {
    Name = "server for web"
    Env  = "dev"
  }
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

# resource "aws_subnet" "main" {
#   vpc_id     = data.aws_vpc.selected.id
#   cidr_block = "10.10.22.128/25"

#   tags = {
#     Name = "Main"
#   }
# }

# resource "aws_subnet" "test-subnet" {
#   vpc_id            = data.aws_vpc.selected.id
#   availability_zone = "ap-southeast-1"
#   cidr_block        = cidrsubnet(data.aws_vpc.selected.cidr_block, 4, 1)
# }